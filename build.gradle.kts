import de.undercouch.gradle.tasks.download.Download
import io.gitlab.arturbosch.detekt.CONFIGURATION_DETEKT
import io.gitlab.arturbosch.detekt.Detekt
import io.gitlab.arturbosch.detekt.getSupportedKotlinVersion
import org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinJvmCompile

plugins {
    java
    application
    `project-report`
    alias(libs.plugins.spring.boot)
    alias(libs.plugins.graalvm)
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.kotlin.spring)
    alias(libs.plugins.git.props)
    alias(libs.plugins.versions)
    alias(libs.plugins.catalog.update)

    alias(libs.plugins.jib)

    alias(libs.plugins.download)

    alias(libs.plugins.spotless)
    alias(libs.plugins.detekt)
}

apply(plugin = "io.spring.dependency-management")

group = "uk.co.borismorris.speedtest"
val mainClassKt = "uk.co.borismorris.speedtest.SpeedtestKt"

java {
    sourceCompatibility = JavaVersion.VERSION_21
    targetCompatibility = JavaVersion.VERSION_21
}

tasks.withType<KotlinJvmCompile> {
    compilerOptions {
        jvmTarget.set(JvmTarget.JVM_21)
        freeCompilerArgs.addAll(
            "-Xjsr305=strict",
            "-Xinline-classes",
        )
    }
}

kotlin {
    sourceSets.all {
        languageSettings {
            languageVersion = "2.0"
            progressiveMode = true
            optIn("kotlin.RequiresOptIn")
            optIn("kotlin.ExperimentalUnsignedTypes")
            optIn("kotlin.ExperimentalStdlibApi")
            optIn("kotlin.time.ExperimentalTime")
            optIn("kotlin.io.path.ExperimentalPathApi")
        }
    }
}

testing {
    suites {
        val test by getting(JvmTestSuite::class)
        val integrationTest by registering(JvmTestSuite::class) {
            testType = TestSuiteType.INTEGRATION_TEST

            sources {
                compileClasspath += test.sources.output
                runtimeClasspath += test.sources.output
            }

            dependencies {
                implementation(project())
            }

            targets {
                all {
                    testTask.configure {
                        shouldRunAfter(test)
                    }
                }
            }
        }

        withType<JvmTestSuite> {
            useJUnitJupiter()
            targets {
                all {
                    testTask.configure {
                        testLogging {
                            exceptionFormat = FULL
                            showStandardStreams = true
                            events("skipped", "failed")
                        }
                    }
                }
            }
        }
    }
}

val integrationTestImplementation by configurations.getting {
    extendsFrom(configurations.testImplementation.get())
}
val integrationTestRuntimeOnly by configurations.getting {
    extendsFrom(configurations.testRuntimeOnly.get())
}

configurations.all {
    exclude(group = "org.springframework.boot", module = "spring-boot-starter-logging")
    exclude(group = "io.projectreactor.netty", module = "reactor-netty-http-brave")
}

configurations.matching { it.name == CONFIGURATION_DETEKT }.all {
    resolutionStrategy.eachDependency {
        if (requested.group == "org.jetbrains.kotlin") {
            useVersion(getSupportedKotlinVersion())
        }
    }
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-aop")
    implementation("org.springframework.boot:spring-boot-starter-log4j2")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("io.micrometer:micrometer-core")
    implementation("io.micrometer:micrometer-registry-otlp")
    implementation("io.micrometer:micrometer-tracing")
    implementation("io.micrometer:micrometer-tracing-bridge-otel")
    implementation("io.opentelemetry:opentelemetry-exporter-otlp")
    implementation("io.opentelemetry:opentelemetry-sdk-extension-autoconfigure")
    runtimeOnly(libs.otel.resources)

    implementation(libs.pyroscope)

    implementation(libs.influx.client)

    implementation(libs.kotlin.logging)
    implementation(libs.slack.appender)
    implementation(libs.httpclient.slack.client)

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testImplementation("org.junit.jupiter:junit-jupiter-params")
    testImplementation(libs.assertj)
    testImplementation("org.awaitility:awaitility")
    testImplementation(libs.wiremock)
    testImplementation(kotlin("test-junit5"))

    integrationTestImplementation(platform(libs.testcontainers))
    integrationTestImplementation("org.testcontainers:junit-jupiter")
    integrationTestImplementation("org.testcontainers:influxdb")
    integrationTestImplementation(libs.influx.flux)
}

spotless {
    kotlin {
        ktlint().editorConfigOverride(
            mapOf(
                "ktlint_standard_no-wildcard-imports" to "disabled",
                "ktlint_standard_max-line-length" to "disabled",
            ),
        )
    }
    kotlinGradle {
        ktlint()
    }
}

val downloadDetektConfig by tasks.registering(Download::class) {
    src("https://gitlab.com/bmorris591/detekt/-/raw/main/detekt.yaml?inline=false")
    dest(layout.buildDirectory.file("detket.config"))
    onlyIfModified(true)
    useETag("all")
}

detekt {
    buildUponDefaultConfig = true
    allRules = true
    config.from(files(downloadDetektConfig.get().dest))
}

tasks.withType<Detekt> {
    jvmTarget = JvmTarget.JVM_20.target
    dependsOn(downloadDetektConfig)
}

tasks["check"].dependsOn("detektMain")

application {
    mainClass.set(mainClassKt)
    applicationDefaultJvmArgs =
        listOfNotNull(
            "-XX:+DisableAttachMechanism",
            "-Dcom.sun.management.jmxremote",
            "-Dcom.sun.management.jmxremote.port=9000",
            "-Dcom.sun.management.jmxremote.local.only=false",
            "-Dcom.sun.management.jmxremote.authenticate=false",
            "-Dcom.sun.management.jmxremote.ssl=false",
            "-Dcom.sun.management.jmxremote.rmi.port=9000",
            "-Djava.rmi.server.hostname=127.0.0.1",
        )
}

jib {
    from {
        image = "azul/zulu-openjdk:21-jre-headless"
    }
    container {
        appRoot = "/opt/speedtest"
        workingDirectory = "/opt/speedtest"
        val javaToolOptions =
            listOfNotNull(
                "-XX:InitialRAMPercentage=50",
                "-XX:MaxRAMPercentage=85",
            ).joinToString(separator = " ")
        environment =
            mapOf(
                "JAVA_TOOL_OPTIONS" to javaToolOptions,
                "SPRING_PROFILES_ACTIVE" to "prod",
            )
        jvmFlags = listOfNotNull("-XX:+PrintCommandLineFlags", "-XX:+DisableExplicitGC")
        mainClass = mainClassKt
        creationTime.set("USE_CURRENT_TIMESTAMP")
    }
}
