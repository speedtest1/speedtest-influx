package uk.co.borismorris.speedtest.native

import com.influxdb.client.InfluxDBClient
import com.influxdb.client.InfluxDBClientFactory
import com.influxdb.client.QueryApi
import com.influxdb.query.dsl.Flux
import org.assertj.core.api.Assertions.assertThat
import org.awaitility.Awaitility.await
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.InfluxDBContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName
import uk.co.borismorris.speedtest.Speedtest
import java.time.Duration
import java.time.temporal.ChronoUnit

private const val SPEEDTESTS = "speedtests"

@SpringBootTest(
    classes = [Speedtest::class],
    properties = [
        "pyroscope.enabled=false",
    ],
)
@Testcontainers
internal class SpeedtestIntegrationTest {
    companion object {
        val influxImage = DockerImageName.parse("influxdb")

        @Container
        @JvmStatic
        val influxdb = InfluxDBContainer(influxImage)
            .withBucket(SPEEDTESTS)
            .withAdminToken("abc123")
            .withEnv("INFLUXD_LOG_LEVEL", "debug")

        @JvmStatic
        @DynamicPropertySource
        fun influxProperties(registry: DynamicPropertyRegistry) {
            registry.add("influx.url") { influxdb.url }
            registry.add("influx.org") { influxdb.organization }
            registry.add("influx.bucket") { influxdb.bucket }
            registry.add("influx.token") { influxdb.adminToken.orElseThrow() }
            registry.add("speedtest.speed-test-executable") {
                if (System.getProperty("os.name").lowercase().contains("win")) {
                    "cmd,/C,src\\\\test\\\\resources\\\\mock-speedtest"
                } else {
                    "src/test/resources/mock-speedtest"
                }
            }
        }
    }

    lateinit var influxDbClient: InfluxDBClient
    lateinit var queryApi: QueryApi

    @BeforeEach
    fun setup() {
        influxDbClient = InfluxDBClientFactory.create(
            influxdb.url,
            influxdb.adminToken.map { it.toCharArray() }.orElseThrow(),
            influxdb.organization,
            influxdb.bucket,
        )
        queryApi = influxDbClient.queryApi
    }

    @AfterEach
    fun teardown() {
        influxDbClient.close()
    }

    @Test
    fun `Speedtest results are submitted to the database`() {
        val flux = Flux.from(SPEEDTESTS).range(-10, ChronoUnit.YEARS).toString()
        await()
            .atMost(Duration.ofMinutes(5))
            .pollInterval(Duration.ofSeconds(5))
            .untilAsserted {
                val tables = queryApi.query(flux)
                println(tables)
                val results = tables.asSequence().flatMap { it.records }.toList()
                assertThat(results).hasSize(8)
            }
    }
}
