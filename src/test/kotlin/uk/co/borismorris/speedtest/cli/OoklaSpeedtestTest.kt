package uk.co.borismorris.speedtest.cli

import com.fasterxml.jackson.databind.ObjectMapper
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import uk.co.borismorris.speedtest.Speedtest

@SpringBootTest(
    classes = [Speedtest::class],
    properties = [
        "pyroscope.enabled=false",
    ],
)
@ActiveProfiles("test")
internal class OoklaSpeedtestTest {

    @Autowired
    private lateinit var mapper: ObjectMapper

    @Test
    fun `A speedtest json deserialises sucessfully`() {
        val speed = this::class.java.getResourceAsStream("speedtest.json").use {
            mapper.readValue(it, OoklaInternetSpeed::class.java)
        }

        assertThat(speed).isNotNull()
    }
}
