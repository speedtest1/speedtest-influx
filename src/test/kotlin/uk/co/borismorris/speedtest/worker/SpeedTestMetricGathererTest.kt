package uk.co.borismorris.speedtest.worker

import com.github.tomakehurst.wiremock.client.WireMock.exactly
import com.github.tomakehurst.wiremock.client.WireMock.findUnmatchedRequests
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import com.github.tomakehurst.wiremock.client.WireMock.verify
import com.github.tomakehurst.wiremock.common.Slf4jNotifier
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import com.github.tomakehurst.wiremock.junit5.WireMockExtension
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import uk.co.borismorris.speedtest.Speedtest

@SpringBootTest(
    classes = [Speedtest::class],
    properties = [
        "pyroscope.enabled=false",
    ],
)
@ActiveProfiles("test")
internal class SpeedTestMetricGathererTest {

    companion object {
        @JvmStatic
        @RegisterExtension
        val wiremock: WireMockExtension = WireMockExtension.newInstance()
            .options(
                WireMockConfiguration.options()
                    .dynamicPort()
                    .usingFilesUnderClasspath("uk/co/borismorris/speedtest/worker/wiremock")
                    .notifier(Slf4jNotifier(true)),
            )
            .configureStaticDsl(true)
            .build()

        @JvmStatic
        @DynamicPropertySource
        fun influxProperties(registry: DynamicPropertyRegistry) {
            registry.add("influx.url") {
                wiremock.baseUrl()
            }
            registry.add("speedtest.speed-test-executable") {
                if (System.getProperty("os.name").lowercase().contains("win")) {
                    "cmd,/C,src\\\\test\\\\resources\\\\mock-speedtest"
                } else {
                    "src/test/resources/mock-speedtest"
                }
            }
        }
    }

    @Autowired
    lateinit var metricGatherer: SpeedTestMetricGatherer

    @Test
    fun `Speedtest results are submitted to the database`() {
        metricGatherer.executeSpeedTest()

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/write")))

        val unmatched: List<*> = findUnmatchedRequests()
        assertThat(unmatched).isEmpty()
    }
}
