package uk.co.borismorris.speedtest.cli

import java.math.BigDecimal
import java.time.Instant

interface SpeedTester {
    fun testInternetSpeed(): InternetSpeed
}

interface InternetSpeed {
    val type: String
    val timestamp: Instant
    val ping: Ping
    val download: TransferSpeed
    val upload: TransferSpeed
    val isp: String
    val iface: TestInterface
    val server: TestServer
    val result: TestResultLink
}

interface Ping {
    val jitter: BigDecimal
    val latency: BigDecimal
}

interface TransferSpeed {
    val bytes: BigDecimal
    val elapsed: BigDecimal
    val bandwidth: BigDecimal
}

interface TestInterface {
    val internalIp: String
    val externalIp: String
    val macAddr: String
    val name: String
    val isVpn: Boolean
}

interface TestServer {
    val id: Long
    val name: String
    val location: String
    val country: String
    val host: String
    val port: Int
    val ip: String
}

interface TestResultLink {
    val id: String
    val url: String
}
