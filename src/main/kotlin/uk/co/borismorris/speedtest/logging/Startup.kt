package uk.co.borismorris.speedtest.logging

import io.github.oshai.kotlinlogging.KotlinLogging
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.boot.info.GitProperties
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.event.EventListener
import uk.co.borismorris.speedtest.conf.InfluxDB2Properties
import uk.co.borismorris.speedtest.conf.MetricCollectionConfig
import uk.co.borismorris.speedtest.conf.SpeedTestConfig

private val logger = KotlinLogging.logger {}

class LogConfigOnStartup(private val context: ConfigurableApplicationContext) {
    @EventListener(ApplicationStartedEvent::class)
    fun onStartup() {
        context.getBean(SpeedTestConfig::class.java).also { logger.info { "speedtestConfig -> $it" } }
        context.getBean(InfluxDB2Properties::class.java).also { logger.info { "influxConfig -> $it" } }
        context.getBean(MetricCollectionConfig::class.java).also { logger.info { "metricsConfig -> $it" } }
    }
}

class LogGitInfoOnStartup(val gitProperties: GitProperties?) {
    @EventListener(ApplicationStartedEvent::class)
    fun onStartup() {
        gitProperties?.apply {
            logger.info { "$branch@$commitId" }
        }
    }
}
