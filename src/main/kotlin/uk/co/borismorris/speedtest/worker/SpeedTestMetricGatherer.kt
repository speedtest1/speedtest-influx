package uk.co.borismorris.speedtest.worker

import com.influxdb.client.domain.WritePrecision
import com.influxdb.client.write.WriteParameters
import io.github.oshai.kotlinlogging.KotlinLogging
import io.micrometer.observation.Observation
import io.micrometer.observation.ObservationRegistry
import org.springframework.scheduling.annotation.Scheduled
import uk.co.borismorris.speedtest.cli.SpeedTester
import uk.co.borismorris.speedtest.influx.InfluxClient
import uk.co.borismorris.speedtest.influx.SpeedtestPointConverter.toPoint

private val logger = KotlinLogging.logger {}

class SpeedTestMetricGatherer(
    private val observationRegistry: ObservationRegistry,
    private val speedTester: SpeedTester,
    private val influxClient: InfluxClient,
) {

    @Scheduled(fixedRateString = "\${metric.period}")
    fun executeSpeedTest() {
        Observation.createNotStarted("execute-speed-test", observationRegistry)
            .observe {
                val speed = speedTester.testInternetSpeed()
                logger.info { "Speed: $speed" }
                influxClient.writePoints(listOf(speed.toPoint()), WriteParameters(WritePrecision.MS, null))
            }
    }
}
