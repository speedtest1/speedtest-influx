package uk.co.borismorris.speedtest.conf

import org.springframework.boot.context.properties.ConfigurationProperties
import java.time.Duration

@ConfigurationProperties(prefix = "speedtest")
@JvmRecord
data class SpeedTestConfig(val speedTestExecutable: List<String>)

@ConfigurationProperties(prefix = "metric")
@JvmRecord
data class MetricCollectionConfig(val period: Duration)

@ConfigurationProperties(prefix = "influx")
class InfluxDB2Properties {
    /**
     * URL to connect to InfluxDB.
     */
    var url: String? = null

    /**
     * Token to use for the authorization.
     */
    var token: String? = null

    /**
     * Default destination organization for writes and queries.
     */
    var org: String? = null

    /**
     * Default destination bucket for writes.
     */
    var bucket: String? = null
    override fun toString() = "InfluxDB2Properties(url=$url, token=XXX, org=$org, bucket=$bucket)"
}
