package uk.co.borismorris.speedtest

import com.fasterxml.jackson.databind.ObjectMapper
import com.influxdb.client.InfluxDBClientOptions
import io.micrometer.observation.ObservationRegistry
import io.pyroscope.javaagent.api.ConfigurationProvider
import org.springframework.boot.WebApplicationType
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.info.GitProperties
import org.springframework.boot.runApplication
import org.springframework.boot.web.client.ClientHttpRequestFactories
import org.springframework.boot.web.client.ClientHttpRequestFactorySettings
import org.springframework.boot.web.client.RestClientCustomizer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.http.client.SimpleClientHttpRequestFactory
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.client.RestClient
import uk.co.borismorris.speedtest.cli.OoklaSpeedtest
import uk.co.borismorris.speedtest.cli.SpeedTester
import uk.co.borismorris.speedtest.conf.InfluxDB2Properties
import uk.co.borismorris.speedtest.conf.MetricCollectionConfig
import uk.co.borismorris.speedtest.conf.SpeedTestConfig
import uk.co.borismorris.speedtest.influx.InfluxClient
import uk.co.borismorris.speedtest.influx.RestClientInfluxClient
import uk.co.borismorris.speedtest.logging.LogConfigOnStartup
import uk.co.borismorris.speedtest.logging.LogGitInfoOnStartup
import uk.co.borismorris.speedtest.pyroscope.Pyroscope
import uk.co.borismorris.speedtest.pyroscope.SpringConfigProvider
import uk.co.borismorris.speedtest.worker.SpeedTestMetricGatherer
import java.time.Duration

@SpringBootApplication
@EnableConfigurationProperties(SpeedTestConfig::class, MetricCollectionConfig::class, InfluxDB2Properties::class)
class Speedtest {

    @Bean
    fun influxConfig(properties: InfluxDB2Properties) = InfluxDBClientOptions.builder()
        .apply {
            properties.url?.let { url(it) }
            properties.token?.let { authenticateToken(it.toCharArray()) }
        }
        .bucket(properties.bucket)
        .org(properties.org)
        .build()

    @Bean
    fun restClientCustomizer() = RestClientCustomizer {
        it.requestFactory(
            ClientHttpRequestFactories.get(
                SimpleClientHttpRequestFactory::class.java,
                ClientHttpRequestFactorySettings.DEFAULTS
                    .withConnectTimeout(Duration.ofMinutes(5))
                    .withReadTimeout(Duration.ofMinutes(5)),
            ),
        )
    }

    @Bean
    fun influxClient(influxConfig: InfluxDBClientOptions, restClientBuilder: RestClient.Builder) =
        RestClientInfluxClient(restClientBuilder, influxConfig)

    @Bean
    fun ooklaSpeedTester(speedTestConfig: SpeedTestConfig, objectMapper: ObjectMapper) =
        OoklaSpeedtest(objectMapper, speedTestConfig)

    @Bean
    fun metricGatherer(observationRegistry: ObservationRegistry, speedTester: SpeedTester, influxClient: InfluxClient) =
        SpeedTestMetricGatherer(observationRegistry, speedTester, influxClient)

    @Bean
    fun logConfigOnStartup(context: ConfigurableApplicationContext) = LogConfigOnStartup(context)

    @Bean
    fun logGitInfoOnStartup(gitProperties: GitProperties?) = LogGitInfoOnStartup(gitProperties)

    @Configuration
    @Profile("!test")
    @EnableScheduling
    class Scheduling

    @Configuration
    @ConditionalOnProperty(prefix = "pyroscope", name = ["enabled"], matchIfMissing = true)
    class PyroscopeConfig {
        @Bean
        fun springConfigProvider(environment: Environment) = SpringConfigProvider(environment)

        @Bean
        fun pyroscope(configurationProvider: ConfigurationProvider) = Pyroscope(configurationProvider)
    }
}

@Suppress("TooGenericExceptionCaught", "SpreadOperator")
fun main(args: Array<String>) {
    runApplication<Speedtest>(*args) {
        webApplicationType = WebApplicationType.NONE
    }
}
