rootProject.name = "speedtest-influx"

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

dependencyResolutionManagement {
    repositories {
        mavenCentral()
        maven("https://repo.spring.io/milestone")
        maven("https://gitlab.com/api/v4/projects/26731120/packages/maven")
    }
}
